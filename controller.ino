#include <AccelStepper.h>
#define FASTADC 1
 

// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif



//Max analog pins to read
#define MAX_PINS 3

// Define stepper motor connections:
#define dirPin 2
#define stepPin 3


AccelStepper stepper(AccelStepper::DRIVER, stepPin, dirPin);


float last_IO_with_RP = millis();


//Variables to contro step motor
unsigned int cycle_time = 0;
int dir = 0;
int xsteps = 0;
int steps_count = 0;
int xsteps_cycle = 0;



float speed_rpm = 0.0;
float speed_I = 0.0;
float speed_E = 0.0;


float cycle_time_clock = millis();
float steps_per_turn = 800.0; //For full steps


//Variables to receive data from serial
char d_or_s = '-';     //variable to know which array is filling up

const byte numChars = 128;//16CHARS X 8

char receivedChars_SI[numChars];   // an array to store the received data
char receivedChars_SE[numChars];   // an array to store the received data
char receivedChars_X[numChars];   // an array to store the received data

String r_SI = "";
String r_SE = "";
String r_X = "";

static byte ndx_X = 0; //Index por positioning array received
static byte ndx_SI = 0; //Index por positioning array received
static byte ndx_SE = 0; //Index por positioning array received


//For reading analog inputs
int n[MAX_PINS];
int sensor_index = 0;
int a[MAX_PINS];

int oversamples = 16;
int shifting = 2;

int sensor_reads_count = 0;
unsigned int sum[MAX_PINS];
float read_data_clock = millis();
float send_data_clock = millis();
float read_data_interval = 10.0;
float send_data_interval = 30.0;



void rotate_motor();
void read_all_pins();
void read_all_pins();
void read_serial();
void traduce_serial();


void setup()
   {
     Serial.begin(250000);
     pinMode(stepPin, OUTPUT);
     pinMode(dirPin, OUTPUT);

     // Set the spinning direction CW/CCW:
     digitalWrite(dirPin, LOW);

     stepper.setAcceleration(11200.0);
     stepper.setMinPulseWidth(25);

     #if FASTADC
     // set prescale to 16
     sbi(ADCSRA,ADPS2) ;
     cbi(ADCSRA,ADPS1) ;
     cbi(ADCSRA,ADPS0) ;
     #endif

   }

void loop()
   {

     rotate_motor();
     read_all_pins();


     if(send_data_clock + send_data_interval < millis()){

        send_data_to_serial();
        send_data_clock = millis();

     }

     //Order matters
     read_serial();
     traduce_serial();



   }




 void read_serial(){

     char endMarker = '\n';
     char rc;

     while(Serial.available()) {

       stepper.run(); //****  STEP MOTOR *****

       rc = Serial.read();
       last_IO_with_RP = millis();


       if (rc != endMarker) {

            if(rc == 'I' || (d_or_s == 'I' && rc != 'D' && rc != 'X' && rc != 'E')){
              d_or_s = 'I';
              receivedChars_SI[ndx_SI] = rc;
              stepper.run(); //****  STEP MOTOR *****

              ndx_SI++;
              if (ndx_SI >= numChars) {
                    ndx_SI = numChars - 1;
              }
            }
            else if(rc == 'E' || (d_or_s == 'E' && rc != 'D' && rc != 'X' && rc != 'I')){
              d_or_s = 'E';
              receivedChars_SE[ndx_SE] = rc;
              stepper.run(); //****  STEP MOTOR *****

              ndx_SE++;
              if (ndx_SE >= numChars) {
                    ndx_SE = numChars - 1;
              }

            }
            else if(rc == 'X' || (d_or_s == 'X' && rc != 'D' && rc != 'I' && rc != 'E')){
              d_or_s = 'X';
              receivedChars_X[ndx_X] = rc;
              stepper.run(); //****  STEP MOTOR *****

              ndx_X++;
              if (ndx_X >= numChars) {
                    ndx_X = numChars - 1;
              }

            }

       }
        else {

            stepper.run(); //****  STEP MOTOR *****

            receivedChars_SI[ndx_SI] = '\0'; // terminate the string
            receivedChars_SE[ndx_SE] = '\0'; // terminate the string
            receivedChars_X[ndx_X] = '\0'; // terminate the string
            ndx_SI = 0;
            ndx_SE = 0;
            ndx_X = 0;


            r_SI = String(receivedChars_SI);
            r_SE = String(receivedChars_SE);
            r_X = String(receivedChars_X);

        }
     }

 }

 void traduce_serial(){


      if(r_SI.length() > 0 && r_SE.length() > 0 && r_X.length() > 0){


           if(r_SI.substring(0,1) == "I"){
              speed_I = (r_SI.substring(1)).toFloat();
              stepper.run(); //****  STEP MOTOR *****

            }
           if(r_SE.substring(0,1) == "E"){
              speed_E = (r_SE.substring(1)).toFloat();
              stepper.run(); //****  STEP MOTOR *****

            }
           if(r_X.substring(0,1) == "X"){
              xsteps = (r_X.substring(1)).toInt();
              stepper.run(); //****  STEP MOTOR *****

            }


            r_SI = String("");
            r_SE = String("");
            r_X = String("");

      }

     float l_IO = millis() - last_IO_with_RP;
     bool stop = false;

     if(l_IO > 3000){
      stop = true;
     }


    //start cycle but only if it has comunicaction
     if(dir == 0 && xsteps_cycle <= steps_count && stop == false && speed_I > 0){

        cycle_time = int(millis() - cycle_time_clock);
        cycle_time_clock = millis();

        dir = 1;
        steps_count = 0;
        speed_rpm = speed_I;
        xsteps_cycle = xsteps;
     }
     //finish cycle with same steps as initial and even if lost communication
     else if(dir == 1 && xsteps_cycle <= steps_count){

        cycle_time = int(millis() - cycle_time_clock);
        cycle_time_clock = millis();
        dir = 0;
        steps_count = 0;
        speed_rpm = speed_E;

     }//if no comunication and cycle has finished, then stop
     else if(dir == 0 && xsteps_cycle <= steps_count && stop == true){

      dir = 0;
      steps_count = 0;
      xsteps_cycle = 0;
      speed_I = 0.0;
      speed_E = 0.0;
      speed_rpm = 0.0;
      cycle_time = 0;
      cycle_time_clock = millis();
     }

   }

void send_data_to_serial(){

      Serial.print("{");
      stepper.run(); //****  STEP MOTOR *****

      for (int i=0; i<MAX_PINS; i++){

        Serial.print("'a");
        Serial.print(i);
        Serial.print("':");
        Serial.print((a[i]));
        stepper.run(); //****  STEP MOTOR *****

        if(i < MAX_PINS){ // i+1 if don want to send dt *
          Serial.print(",");

        }
      }


      int timestamp = 0;
      if(millis() > 10000){
        timestamp = millis() % 10000;
      }
      else{
        timestamp = millis();
      }

      stepper.run();

      Serial.print("'D':");  //*
      Serial.print(dir);     //*
      stepper.run(); //****  STEP MOTOR *****

      Serial.print(",'I':");  //*
      Serial.print(speed_I);    //*
      stepper.run(); //****  STEP MOTOR *****

      Serial.print(",'E':");  //*
      Serial.print(speed_E);    //*
      stepper.run(); //****  STEP MOTOR *****

      Serial.print(",'X':");  //*
      Serial.print(xsteps);    //*
      stepper.run(); //****  STEP MOTOR *****

      Serial.print(",'TS':");  //*
      Serial.print(timestamp);    //*
      stepper.run(); //****  STEP MOTOR *****

      Serial.print(",'C':");  //*
      Serial.print(cycle_time);    //*
      stepper.run(); //****  STEP MOTOR *****

      Serial.print(",'Xc':");  //*
      Serial.print(steps_count);    //*
      stepper.run(); //****  STEP MOTOR *****

      Serial.println("}");
      stepper.run(); //****  STEP MOTOR *****



}


void read_all_pins(){

      int a_oversampled = 0;

      for(int i=0; i<oversamples; i++){

        a_oversampled += analogRead(sensor_index);
        stepper.run(); //****  STEP MOTOR *****

      }

      a_oversampled = a_oversampled >> shifting;

      sum[sensor_index] += a_oversampled;
      n[sensor_index] += 1;

      if(sensor_index >= MAX_PINS){
        sensor_index = 0;
        sensor_reads_count += 1;
      }
      else{
        sensor_index += 1;
      }


      if(read_data_clock + read_data_interval < millis()){

        sensor_reads_count = 0;

        for(int i=0; i<MAX_PINS; i++){

           float accom = 0.0;

           if(a[i] > 0){
              accom = a[i];
           }

           a[i] = (sum[i]/n[i] + accom)/2;
           stepper.run(); //****  STEP MOTOR *****

        }

        read_data_clock = millis();

        for(int i=0; i<MAX_PINS; i++){

            n[i] = 0;
            sum[i] = 0;
            stepper.run(); //****  STEP MOTOR *****

         }
     }
 }

   void rotate_motor(){

      if(fabs(speed_rpm - 0.0) > 0.001){

          //X100I15E7.5 //X200E45.99I99.99
          //X100I30E15
          //X100I60E30
          //X400I60E30

          int steps_elapsed = 0;

          if(dir == 1){
                 stepper.moveTo((long) xsteps_cycle);
                 steps_elapsed = 0;

              }
          else if(dir == 0){
            stepper.moveTo((long) 0);
            steps_elapsed = xsteps_cycle;

          }

          stepper.setMaxSpeed(speed_rpm * steps_per_turn * 1.05 / 60.0);
          stepper.run();
          steps_count = abs(steps_elapsed - stepper.currentPosition());


      }





   }
