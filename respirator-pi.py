
import time
import random
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.animation as animation
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tkinter as Tk
import json
import sys
import math
from multiprocessing import Process, Pipe
import threading

from classes.control import CONTROL
from classes.readwriteserial import READWRITESERIAL
from classes.gui import GUI




SERIAL_NAME = "/dev/ttyACM0"
EXIT_LOOP   = False  #Variable to indicate if processes and threads should end
INTERVAL    = 50     #ms for updating plots
DATA_PLOT_LIMIT = 20 #seconds for show in X axis.
MOTOR_STEPS = 800    #steps for a full turn

COMAND_INIT = {"I":0.0, "E":0.0, "X":0}  #Initial instruction to arduino

#Parameters to create graphic interface
#first key represents the order in which it is draw
PARAMETERS = \
{"0": \
    {"tag": "Freq (RPM)", "step": 1, "default": "20", \
     "max": 40, "min": 20, "fun": "F"}, \
 "1": \
    {"tag": "VOL INSP", "step": 1, "default": "350", \
     "max":150, "min": 1, "fun": "V"}, \
 "2": \
    {"tag": "Pressure", "step": 1, "default": "25", \
     "max":30,  "min": 1, "fun": "P"}, \
 "3": \
    {"tag": "PEEP", "step": 1, "default": "5",  \
     "max":15, "min": 0, "fun": "PE"}, \
 "4": \
    {"tag": "Trigger", "step": 2, "default": "-2", \
     "max":0, "min": -6, "fun": "T"}, \
 "5": \
    {"tag": "I:E", "step": 0.1, "default": "2.0", \
     "max":4, "min": 1, "fun": "I_E"}}

#Parameters to create plots.
#First key indicates the order in which it will be plotted
GRAPH_PARAMS = \
    {"0": {"FLOW":     {"max":60, "min": -60, "major": 10, "minor": 5 }}, \
     "1": {"PRESSURE": {"max":50,  "min": -10, "major": 5, "minor": 1 }},\
     "2": {"VOLUME":   {"max":1000, "min": 0, "major": 100, "minor": 25 }}}


#After adjusting V at 0kpa and converting it to mmh20 - 106 was the offset
#to make a 0 reading
manual_pressure_zero = 1.856#18.57 #106
psi_to_mmh2o = 703.08893732448
psi_to_mmhg  =  51.7149
pa_to_cmh2o = 0.010197162129779
psi_to_cmh2o = 70.308893732448

#cross section area of the sensor tube printed
TUBE_SENSOR_AREA = math.pi * (0.023/2)**2  #METERS  #0.0408 #018
#Once turned on, the ADC reading at 0wind shows 1.6V
ZeroWind_V = 1.636   #1.346 according to datasheet
ZeroTemp_V = 0.40    #spec sheet says 0.40 -
MPH_to_MS =  0.44704 #mph to m/s

#Important
#The MAX functions are added to never prevent dividing by cero or
#calculate negative root values
digital_to_V = lambda x: x / 4095 * 5  #4096 = 12bits
temp_formula =  lambda x: (max(0, digital_to_V(x[0]) - ZeroTemp_V)) / 0.0195

#Flow formula rounded to 1 decimal and converted to L/M
#It receives an array of the 2 sensors connected. TEMP,FLOW
flow_formula = lambda x:  \
        round( \
             ( ((max(0,(digital_to_V(x[0]) - ZeroWind_V)) / \
              (3.038517 * max(1,(temp_formula([x[1]]) ** 0.115157 )))) \
              / 0.087288 ) ** 3.009364 ) * \
             MPH_to_MS * TUBE_SENSOR_AREA * \
             60000, 1)

#Pressure formula
pressure_formula = \
        lambda x: round((digital_to_V(x[0])/5 - 0.50) / 0.057 * 1000 * \
                    pa_to_cmh2o - manual_pressure_zero, 1)


#Initial parameters for graphing
#Names should be same as values to read from sensors
Y_DATA_GLOBAL = {"g0": [0], "g1": [0], "g2": [0]}

#Inputs from sensors name
#Main keys of dict are for graphing, which values to take from self.j in
#readwriteserial.py
#G0 will print in graph 0
#G1 will print in graph 1
#G2 will print in graph 2
#As specified above in GRAPH_PARAMS
#a0 temp
#a1 flow
#a2 press
#Inputs is an array of the data input needed to calculate the formulas
#Adjust is to determine if any special function should be made once the
#sensor data is received in readwriteserial.py
#conver is the sensor formula
SENSORS_PARAMS = \
    {"g1": {"inputs": ["a2"], \
            "adjust": False, \
            "convert": pressure_formula}, \
     "g0": {"inputs": ["a1", "a0"], \
            "adjust": "rate", \
            "convert": flow_formula}, \
     "g2": {"inputs": ["a1", "a0"], \
            "adjust": "qty", \
            "convert": flow_formula}}




#-------
lastT = time.time()              #I use this variable to test delays
#-------

def runGraph(parent_conn):

    global INTERVAL
    global DATA_PLOT_LIMIT
    global GRAPH_PARAMS
    global PARAMETERS
    global COMAND_INIT
    global SENSORS_PARAMS
    global Y_DATA_GLOBAL


    print("Graph process started!")

    plt.style.use('seaborn-pastel')

    # create a figure with two subplots
    fig, (ax0, ax1, ax2) = plt.subplots(3,1, \
                            figsize=(7.9,7.15))

    ax0.grid()
    ax1.grid()
    ax2.grid()

    ax0.title.set_text(list(GRAPH_PARAMS["0"].keys())[0])
    ax1.title.set_text(list(GRAPH_PARAMS["1"].keys())[0])
    ax2.title.set_text(list(GRAPH_PARAMS["2"].keys())[0])


    # intialize two line objects (one in each axes)
    line0, = ax0.plot([], [], lw=2, color='r')
    line1, = ax1.plot([], [], lw=2, color='b')
    line2, = ax2.plot([], [], lw=2, color='g')
    line = [line0, line1, line2]

    #Interval / 2 to try to make fast readings of the serial I/O
    #eventhough it is interval/2 -> fastest I have reach is 60ms
    readwriteSerial = READWRITESERIAL(COMAND_INIT, SENSORS_PARAMS, \
                                    interval=INTERVAL/3, s_name=SERIAL_NAME)

    #User doesnt care if his instruction is sent 100ms or 500ms after they´ve
    #clicked on it. It loops every 50ms X 10 = 500ms
    control = CONTROL(COMAND_INIT, SENSORS_PARAMS, PARAMETERS, MOTOR_STEPS, \
                                    interval=INTERVAL*10)


    #Loop to initilize plots parameters
    i = 0;
    for ax in [ax0, ax1, ax2]:


        formatter = plt.FuncFormatter( \
                    lambda x, loc: "{:,.0f}".format(int(x)))
        ax.yaxis.set_major_formatter(formatter)
        ax.yaxis.set_major_locator(ticker.MultipleLocator\
                (GRAPH_PARAMS[str(i)][ax.get_title()]["major"]))
        ax.yaxis.set_minor_locator(ticker.MultipleLocator\
                (GRAPH_PARAMS[str(i)][ax.get_title()]["minor"]))

        ax.set_ylim(GRAPH_PARAMS[str(i)][ax.get_title()]["min"], \
                    GRAPH_PARAMS[str(i)][ax.get_title()]["max"])
        ax.set_xlim(0, DATA_PLOT_LIMIT)

        ax.xaxis.set_major_locator(ticker.MultipleLocator(1))

        i += 1


    #Initialize the data arrays
    y_data = Y_DATA_GLOBAL
    xdata  = [0]


    def quit_figure(event):
        if event.key == 'q':

            readwriteSerial.close_arduino()
            control.stop()

            parent_conn.send(True)#EXIT TRUE
            plt.close(event.canvas.figure)

    #This is to try and put together both screens (UI and Plot figure)
    def move_figure(f, x, y):
        backend = matplotlib.get_backend()

        if backend == 'TkAgg':
            f.canvas.manager.window.wm_geometry("+%d+%d" % (x, y))
        elif backend == 'WXAgg':
            f.canvas.manager.window.SetPosition((x, y))
        else:
            # This works for QT and GTK
            # You can also use window.setGeometry
            f.canvas.manager.window.move(x, y)

    #Show in right part of the screen
    move_figure(fig, 700, 40)


    def data_gen():

        t = data_gen.t

        #Read values from the J variable in the read/write thread
        #(contains last sensor data received)
        j = readwriteSerial.get_values()

        #Update J value in control thread to monitor and make some decisions
        control.update_sensors_data(j)

        #If there´s something to send to arduino, then send it to the
        #read/write Thread
        if control.is_new_data_available():
            #print(control.what_to_do())
            readwriteSerial.send_next_instruction(control.what_to_do())
            pass

        #Check if there´s some instructions from the user to send
        #they are passed to the control thread which will then prepared them
        #for sending them to the arduino
        if parent_conn.poll():
            instructions = parent_conn.recv()
            control.instructions(instructions)


        #Lines to test I/O delays between processes.
        #After initialization, communication is fast
        #----------------------------------------------------
        global lastT
        d = time.time() - lastT
        if d > 0.5:
            print("Delay i/o between processes " + str(d), file=sys.stderr)
        lastT = time.time()
        #----------------------------------------------------


        x = (time.time() - t)

        y = j

        yield x, y, t


    #Initialize time 0 for graphing
    data_gen.t = time.time()



    def run(data):

        # update the data
        x, y, t = data

        keys_read = list(y.keys())
        keys_in_y_data = list(y_data.keys())
        #I do not assign y_data directly beacuse it doesnt copy the data
        valid_data = {}
        valid_data.update(y_data)
        x_value = x

        if bool(y) and len(keys_read) > 0:

            for k_read in keys_read:

                last_y_value = 0

                if k_read in keys_in_y_data and len(y_data[k_read]) > 0:
                    last_y_value = y_data[k_read][len(y_data[k_read]) - 1]

                if k_read in keys_in_y_data and \
                    int(x) == DATA_PLOT_LIMIT:

                    y_data[k_read].clear()


                    if int(x) == DATA_PLOT_LIMIT:
                        #x value has to be clenead to for not connecting
                        #last point in graph with first when repeating
                        data_gen.t = time.time()
                        x_value = 0
                        xdata.clear()

                if k_read in keys_in_y_data:
                    y_data[k_read].append(y[k_read])
                    valid_data.pop(k_read)

            xdata.append(x_value)

            #if in serial data some key was missing - add the last value
            if len(list(valid_data.keys())) > 0 :

                for v in list(valid_data.keys()):
                    y_data[v].append(y_data[v][len(y_data[v]) - 1])

            # update the data of both line objects
            for l in range(len(line)):
                line[l].set_data(xdata, y_data["g"+str(l)])


        return line

    print("Creating animation...")
    ani = animation.FuncAnimation(\
                fig, run, data_gen, \
                interval=INTERVAL, \
                blit=True)


    cid = plt.gcf().canvas.mpl_connect('key_press_event', quit_figure)

    plt.tight_layout()
    readwriteSerial.start()  #Start read/write serail thread
    control.start()          #Start control Thread
    plt.show()







if __name__ == '__main__':
    parent_conn, child_conn = Pipe()
    p = Process(target=runGraph, args=(parent_conn,))
    print("Starting graph process...")
    p.start()
    gui = GUI(PARAMETERS, child_conn, INTERVAL)
    p.terminate()
    p.join()
