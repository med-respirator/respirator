

# Respirator | Ventilator

This is a **proof of concept** for a low budget medical ventilator.
Lots of testing, measuring and validations are still missing.

**It is still in progress - it is not finished**.
So far this is what we have. (Code still needs to be
commented)


## Ventilator requirements:
- 200ml to 800ml of tidal air
- Presure control. Max 40 mmH2O for inspiration and 25 cmH2O for expiration
- PEEP frpm 4-25 mmH2O
- Respiration frequency from 8 - 40 per minute.
- I:E  from 1:1 to 1:4
- Air volume supplied
- FiO2 from 21% to 100%
- Trigger by negative pressure

## Materials:
- Wind sensor from: $ 24 USD
https://moderndevice.com/product/wind-sensor-rev-p/

- Pressure sensor from: $ 20 USD
https://moderndevice.com/product/pressure-sensor-mpxv/
(I chose the +- 1Psi)

- Step motor driver TB66000 $ 11 USD

- Nema 23 step motor $ 15 USD

- Arduino UNO  $ 23 USD

- Raspberry Pi +3B $ 45 USD

- adult Ambu bag $ 60 USD

- 12VDC power source


## Useful commands and libraries

To check which port is arduino connected, run the following command in the Raspberry
terminal.
```
ls /dev/tty*
```

There should be an ouput with a 0 or 1 most likely.
/dev/ttyACM0    /dev/ttyACM1    


Install PySerial:
```
pip3 install pyserial
```



# General approach

Raspberry PI handles only the graphics and UI.
Arduino controls the system based on data sent by the raspberry.


The whole Raspberry program is divided in 2 proceses.
- Main process handles UI
- The second process handles the graphs, serial reads and general user control.

The second process creates (apart from the Matplot animation thread) 2 different
threads.
- First Thread handles all the I/O with the serial port
- Second Thread handles what instructions to send to the Arduino

I took this approach to try to plot the fastest and closest to real-time.
On average there´s a 60-150ms (Min-Max) delay from actual readings.

For plotting I believe is good enough.

Since we first tried to recreate Orbitz´s mechanical ventilator idea (before we knew it didnt work), we used what we have done already.

Based on this, we only need to control a motor to make it turn 1/4th of a turn and then back to perform a full breath cycle. The air pressure and volume should be only a function of speeds and motor steps.


## I/O between Arduino and Raspberry
Arduino sends a JSON like text with sensors data, cycle elapsed time for Insp/Exp
and the last instructions received. (Inspiration speed, Expiration speed, motor
steps to do) The data is sent every ~50ms.

If last instruction received was more than 3seconds ago, then it stops and reset
all instructions. (It stops once the whole process has been completed INS/EXP)

Raspberry sends every ~500ms instructions to the Arduino. (Insp speed, Exp speed,
steps to move the motor)

I chose a 250000 baudrate since it improves the I/O speed for reading and writing.

I also change to 1MHZ the ADC readings in the arduino to try and experiment
increasing the resolution of the sensors to 12bits instead of 10bits.


## Limitations
- It is hard to find "cheap" low air flow and low pressure sensors.

So far at 0 pressure and 0 wind:
- Pressure sensor gives an error of ~ +/- 1 mmH2O
- Wind sensor gives and error of ~ +/- 3 L/min


## Functionalities missing
- PEEP control should be possible with a valve connected to the ambu bag
to retain pressure and controlled by the Arduino.
Without that, the ambu bag just releases the air and keeps no pressure.

- Oxigen control.
This may be done appart from the system and perhaps in a small tank connected
with some manual valves and a digital oxigen reader.

- Medical grade filters  

- Separate in/out flows

- Backup battery

- Self calibrating routine for adjusting position 0

## Arduino code
Arduino is programed so that it keeps running independently from the rapsberry. Any I/O latencies
between them should not be important.
If for any reason Arduino stops receiving instructions for +3s it continues to run until the current cycle
is finished then it is set to 0 all instructions.
Arduino receives:
- The inspiration speed ( I )
- The expiration speed ( E )
- The motor step to do in the cycle ( X )

Arduino sends:
- The inspiration speed ( I )
- The expiration speed ( E )
- The motor step to do in the cycle ( X )
- The motor steps count ( Xc )
- The timestamp of when the data was sent ( TS )
- The direction of the motor ( D )
- The last half a cycle time elapsed ( C )
- The sensor´s data ( a0, a1, a2 )


Arduino UNO has a 10bit ADC. Since both the wind and pressure sensor have a relative long range of values for our application, I tried to experiment increasing some how the resolution in the data read.
According to some, one can increase an ADC resolution by oversampling at a high frequency. I tried bringing the resolution to 12bits and since I have no way to confirm it helped, I do have a faster arduino with a signal with low noise.



## Arduino pin connections
- PIN 2 and PIN 3 are for controlling DIR and PUL of step motor.
- A0 for the temperature compensator of the wind sensor
- A1 for the wind sensor
- A2 for pressure sensor

![Arduino connections ](/images/connections.jpg)

![General settings overview ](/images/overview.jpg)

![Graphics and first sensors test ](/images/manual_test.jpg)
**Image shows a test done manually (not with the mechanism)**


## Colaborators
@almilcar Code
@arjiba  3d printing and design
@Guiller03259448 mechanical design & manufacturer
@alejandrolugob1   MD advisor
