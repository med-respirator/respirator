import time
from threading import Thread






class CONTROL:


    def __init__(self, COMAND_INIT, SENSORS_PARAMS,  \
                PARAMS, MOTOR_STEPS, interval):
        self.PARAMETERS = PARAMS
        self.SENSORS_PARAMS = SENSORS_PARAMS
        self.stopped = False
        self.name = "Control thread"
        self.interval = interval
        self.MOTOR_STEPS = MOTOR_STEPS

        self.dir = 0
        #for half cycle, 1/2th turn (200stp)
        self.X = int(self.MOTOR_STEPS/4 + self.MOTOR_STEPS/4/2)

        self.FREQ = 0 #Frequency
        self.I_E = 0  #I:E
        self.V = 0    #Tidal Volume
        self.P = 0    #Pressure
        self.PE = 0   #PEEP
        self.T =0     #Trigger

        #the motor and controller may have some delay in running code
        self.i_o_error = {"1": [], "0": []}
        self.i_o_error_size = 5
        self.i_o_error_value = {"1": 0, "0": 0}
        self.beta = 0.2 #error beta used for control and adding errors


        self.timeout_send = time.time()

        #formula to determine inspiration time
        self.timeout_f_ins = \
                lambda f, i_e: 60/f / (i_e + 1)

        #formula to determine expiration time
        self.timeout_f_exp = \
                lambda f, i_e: 60/f * (i_e)/(i_e + 1)

        self.fill_default_params()      #initialize filling default params

        self.j = {} #values from read/data serial
        self.data_to_send = COMAND_INIT #initial values to 0 sent to arduino
        self.new_data_available = True  #default params ready to be sent to Ard


    def fill_default_params(self):

        for k in self.PARAMETERS:

            if self.PARAMETERS[k]["fun"] == "F":
               self.FREQ = float(self.PARAMETERS[k]["default"])

            if self.PARAMETERS[k]["fun"] == "I_E":
               self.I_E = float(self.PARAMETERS[k]["default"])

            if self.PARAMETERS[k]["fun"] == "V":
               self.V = float(self.PARAMETERS[k]["default"])

            if self.PARAMETERS[k]["fun"] == "P":
               self.P = float(self.PARAMETERS[k]["default"])

            if self.PARAMETERS[k]["fun"] == "PE":
               self.PE = float(self.PARAMETERS[k]["default"])

            if self.PARAMETERS[k]["fun"] == "T":
               self.T = float(self.PARAMETERS[k]["default"])




    def what_to_do(self):
        self.new_data_available = False
        return self.data_to_send

    def is_new_data_available(self):
        return self.new_data_available

    def start(self):

        # start the thread to read frames from the video stream
        t = Thread(target=self.update, name=self.name, args=())
        t.daemon = True
        t.start()
        print("Control thread created...")
        return self

    def stop(self):
         self.stopped = True

    def update_sensors_data(self, j):
        self.j = j

    def update(self):

        print("Control thread started")

        t_out_ins = self.timeout_f_ins(self.FREQ, self.I_E)
        t_out_exp = self.timeout_f_exp(self.FREQ, self.I_E)
        #Initialize to start in control with first parametrs
        self.send_data(self.speed_ins(t_out_ins), self.speed_exp(t_out_exp))
        time.sleep(10)# shoudl wait so the graph can show before start reading
        # keep looping infinitely until the thread is stopped
        while True:
            self.control()
            time.sleep(self.interval/1000)
            # if the thread indicator variable is set, stop the thread
            if self.stopped:
               print("Exiting control thread...")
               return

        return

    def send_data(self, speed_i, speed_e):
        #Only data to send is Inspiration speed I
        #Expiration speed E
        #Steps to do for cycle
        self.new_data_available = True
        self.data_to_send = \
            {"i": speed_i, "e": speed_e, "x":self.X}


    def speed_ins(self, t_out_ins):

        #Half a turn is one cycle -
        conv = self.X / self.MOTOR_STEPS
        e = 1

        if len(self.i_o_error["1"]) == self.i_o_error_size:
            e = 1 + self.i_o_error_value["1"] / t_out_ins
            #print(e)

        return round(60 / t_out_ins * conv * e, 2)

    def speed_exp(self, t_out_exp):

        #Half a turn is one cycle
        conv = self.X  / self.MOTOR_STEPS
        e = 1

        if len(self.i_o_error["0"]) == self.i_o_error_size:
            e = 1 + self.i_o_error_value["0"] / t_out_exp
            #print(e)

        return round(60 / t_out_exp * conv * e, 2)


    def control(self):

         t_out_ins = self.timeout_f_ins(self.FREQ, self.I_E)
         t_out_exp = self.timeout_f_exp(self.FREQ, self.I_E)

         #When dir=1 -> value in C is from EXP (d=0)
         to_show = [t_out_ins, t_out_exp]

         if "D" in self.j and self.j["D"] == self.dir and \
            "C" in self.j:
             self.dir = int(not self.dir);

             indx = str(self.dir)

             t_error = self.j["C"]/1000 - to_show[self.j["D"]]
             e_percentage = t_error / (to_show[self.j["D"]])

             #save errors
             self.i_o_error[indx].append(t_error)

             #dont start to calculate error, until it has done some loops
             if len(self.i_o_error[indx]) > self.i_o_error_size:

                 self.i_o_error[indx].pop(0)
                 avg = (sum(self.i_o_error["0"]) + sum(self.i_o_error["1"]))/ \
                    self.i_o_error_size / sum(to_show)


                 if (self.i_o_error[indx][self.i_o_error_size-1] > 0.035 and \
                     avg > 0.035) or \
                    (self.i_o_error[indx][self.i_o_error_size-1] < - 0.035 and \
                     avg < - 0.035):
                     print("Compensating active..")
                     print("Cycles " + str(round(avg*100,2)) + "% error")

                     #print(self.j)
                     #self.i_o_error_value[indx] =  \
                    #        self.i_o_error_value[indx] + t_error * self.beta



             #print(self.i_o_error_value[indx])






        #Sendo data every 250ms
         if (time.time() - self.timeout_send) > 0.25:
            speed_ins = self.speed_ins(t_out_ins)
            speed_exp = self.speed_exp(t_out_exp)
            self.send_data(speed_ins, speed_exp)
            self.timeout_send = time.time()

            self.new_data_available = True




    def instructions(self, instructions):
         #print(instructions)
         if self.PARAMETERS[instructions["btn"]]["fun"] == "F":

            self.FREQ = instructions["value"]
            print(str(self.FREQ) + " new Freq")
         if self.PARAMETERS[instructions["btn"]]["fun"] == "I_E":

            self.I_E = instructions["value"]
            print(str(self.I_E) + " new I_E")
