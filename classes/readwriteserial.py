#!/usr/bin/env python3


import time
import json
import random
import serial
import math

from threading import Thread

class READWRITESERIAL:

    def __init__(self, COMAND_INIT, SENSORS_PARAMS,  \
    interval=35,s_name="/dev/ttyACM0"): #* try change acm1

        self.stopped = False
        self.reading_serial = False
        self.SENSORS_PARAMS = SENSORS_PARAMS

        self.j = {}
        self.name = "Respirator-serial-reader"
        self.arduino = None
        self.interval = interval

        self.inspiration_expiration = 0
        self.ml_accom = 0

        self.reading_timer = time.time()
        self.accom_inter = []


        #Fill data for local testing
        #self.val_read_data = \
        #"{'a0':0, 'a1':0, 'a2':0, 'a3':0, 'D':0, 'S':0.0, 'X': 0} "

        #clean string to make it json
        #self.val_read_data = self.val_read_data.replace("'",'"')
        self.j = {}#json.loads(self.val_read_data)
        self.send_to_serial_data = COMAND_INIT
        self.time_since_last_write = time.time()

        self.arduino = \
             serial.Serial(s_name, \
             baudrate=250000, timeout=1.5, write_timeout=1.5)

        time.sleep(2)



    def get_values(self):

        return self.j

    def read_from_serial(self):

        self.reading_serial = True
        same_value = False
        valid_json = True

        try:
            #Receives:
            #Sensor values a0, a1,a2
            #Inspiration speed I
            #Expiration speed E
            #Last half cycle time C
            #Steps to perform X
            #Step count Xc
            #Direction of motor D
            #Timestamp of when the data was sent TS


            self.val_read_data = self.arduino.readline().decode("utf-8")
            #clean string to make it json
            self.val_read_data = self.val_read_data.replace("'",'"')
            j = json.loads(self.val_read_data)

            elapsed = 0
            if "D" in j:
                inspiration_expiration = j["D"]
            else:
                valid_json = False

            if "TS" in j and "TS" in self.j:
                elapsed = (j["TS"] - self.j["TS"])/1000
                if elapsed < 0:
                    elapsed += 10 #+1000 / 1000

                #same value read as before
                elif elapsed == 0:
                    same_value = True

            elif "TS" in j:
                elapsed = j["TS"]/1000

            else:
                valid_json = False

            if not same_value and valid_json:
                self.accom_inter.append((time.time() - self.reading_timer))
                self.reading_timer = time.time()

                if inspiration_expiration != self.inspiration_expiration:
                    #self.ml_accom = 0
                    self.inspiration_expiration = inspiration_expiration
                    print(str(sum(self.accom_inter)/len(self.accom_inter)))
                    self.accom_inter = []

                    #print("Ins_Exp " + str(inspiration_expiration))

                j.update({"elapsed": elapsed})

                for indx in self.SENSORS_PARAMS.keys():

                    adjust = self.SENSORS_PARAMS[indx]["adjust"]
                    inputs = self.SENSORS_PARAMS[indx]["inputs"]

                    input_values = list(map(j.get, inputs))

                    if None in input_values:
                        raise ValueError

                    k_value = self.SENSORS_PARAMS[indx]["convert"](input_values)

                    if self.inspiration_expiration == 0 and \
                       (indx == "g0" or indx == "g2"):
                        #print(str(k_value) + " " + indx + " " + str(j["a1"]) + " " \
                        #+ str(j["a0"]))
                        k_value *= -1
                    #    pass


                    j.update({"calc_" + indx: k_value})


                    if ("calc_" + indx) in self.j:
                        k_value = round((k_value + self.j["calc_" + indx])/2)

                        if indx == "g0" or indx == "g1":

                            pass

                        if adjust == "qty":

                            decay = 1
                            l_v = self.j["calc_" + indx]

                            curve_a = (k_value - l_v) * elapsed / 2
                            rect_a = self.j["calc_" + indx] * elapsed

                            if curve_a <= 0 and rect_a <= 0:
                                decay = 4

                            k_value = \
                                max(0, (curve_a + rect_a)  + \
                                self.ml_accom / decay)

                            self.ml_accom = k_value

                            k_value = k_value / 60 * 1000



                    #    if adjust == "rate":
                    #        pass
                                #k_value = (k_value - self.j["calc_" + indx]) / \
                                #           elapsed

                    #else:

                    #    k_value = 0


                    #print(k_value)
                    j.update({indx: k_value})

                self.j.update(j)


                #generates json exceptions but keeps i/o close to real time
                #self.arduino.flushInput()


        except UnicodeDecodeError:
            print('uh oh - UNICODE')

        except ValueError:
            #print('uh oh - JSON')
            pass


        self.reading_serial = False



        return self.j


    def update(self):

       print("ReadWrite thread started!")
       # keep looping infinitely until the thread is stopped
       while True:

            self.read_from_serial()
            self.send_to_serial()
            time.sleep(self.interval/1000)

            if self.stopped:
               #self.send_to_serial()
               print("Exiting thread serial...")
               time.sleep(2)#*
               self.arduino.close()

               return

       return

    def send_next_instruction(self, speed_dir):
        self.send_to_serial_data = speed_dir


    def send_to_serial(self):

        keys = list(self.send_to_serial_data.keys())

        dta = ""
        for i in keys:
            k = i[0].capitalize()

            if self.send_to_serial_data[i] != None:

                if (time.time() - self.time_since_last_write) > \
                 self.interval/1000 * 5: #25ms * 250ms total

                    dta = dta + (k+str(self.send_to_serial_data[i]))


        if dta != "":

            try:
                self.arduino.flushInput()
                self.arduino.write((dta + "\n"). \
                    encode('utf-8'))
                self.time_since_last_write = time.time()

            except serial.SerialTimeoutException:
                print("timeout write")
                self.arduino.flushOutput()





    def start(self):

    # start the thread to read frames from the video stream
        t = Thread(target=self.update, name=self.name, args=())
        t.daemon = True
        t.start()
        print("ReadWrite thread created...")

        return self

    def close_arduino(self):
         self.stopped = True
