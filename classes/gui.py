
import time
import tkinter as Tk


class GUI:

    def __init__(self, parameters, child_conn, interval):

        self.window = Tk.Tk()

        self.parameters = parameters

        self.btn_increase  = []
        self.btn_decrease  = []
        self.lbl_value = []
        self.lbl_name = []

        min_col_size = 125

        self.window.columnconfigure([0, 1, 2, 3], \
                            minsize=min_col_size, weight=1)

        prms_k = list(sorted(self.parameters.keys()))

        for row in range(len(prms_k)):

            self.lbl_name.append(Tk.Label(\
                master=self.window, text=self.parameters[prms_k[row]]["tag"],\
                bg="white", fg="blue", font=("Courier", 14)))

            self.lbl_name[row].grid(row=row, column=0, sticky="nsew")

            self.window.rowconfigure(row, \
                            minsize=min_col_size, weight=1)

            self.btn_decrease.append(Tk.Button(\
                            master=self.window, text="-", \
                            bg="white", fg="blue", font=("Courier", 28), \
                            command=lambda c=row: self.decrease(c)))

            self.btn_decrease[row].grid(row=row, column=1, sticky="nsew")

            self.lbl_value.append(Tk.Label(\
                            master=self.window, \
                            text=self.parameters[prms_k[row]]["default"], \
                            bg="white", fg="blue", font=("Courier", 28)))

            self.lbl_value[row].grid(row=row, column=2, sticky="nsew")

            self.btn_increase.append(Tk.Button(\
                            master=self.window, text="+", \
                            bg="white", fg="blue", font=("Courier", 28), \
                            command=lambda c=row: self.increase(c)))

            self.btn_increase[row].grid(row=row, column=3, sticky="nsew")


        self.window.geometry("+%d+%d" % (700-min_col_size*4, 40))
        self.window.title("RESPIRATOR controller")

        #This is the variable used to connect both processes
        self.child_conn = child_conn

        self.interval = interval
        self.motor_time = time.time()

        # start
        self.lbl_value[len(self.parameters)-1].after(interval*2, self.run)
        self.window.mainloop()


    def increase(self, index):

       value = round(float(self.lbl_value[index]["text"]),1)

       if value == self.parameters[str(index)]["max"]:
           return

       else:
           step = self.parameters[str(index)]["step"]
           v = value + step
           self.lbl_value[index]["text"] = str(round(v, 1))
           self.child_conn.send({"btn": str(index), \
                              "value": v})

    def decrease(self, index):

        value = round(float(self.lbl_value[index]["text"]), 1)

        if value == self.parameters[str(index)]["min"]:
            return

        else:
            step = self.parameters[str(index)]["step"]
            v = value - step
            self.lbl_value[index]["text"] = str(round(v, 1))
            self.child_conn.send({"btn": str(index), \
                              "value": v})


    def run(self):

        EXIT_LOOP = False

        if self.child_conn.poll():
            EXIT_LOOP = self.child_conn.recv()


        if EXIT_LOOP:
            print("Closing GUI...")
            self.window.after(1, self.window.destroy)

        else:
            self.window.after(self.interval, self.run)
